import React from "react";
import CheckmarkIcon from "../../assets/images/CheckMark.png";

function Spinner() {
  return (
    <img
      style={{ marginLeft: 15, maxWidth: 20, maxHeight: 20 }}
      src={CheckmarkIcon}
      alt="checkmark"
    />
  );
}

export default Spinner;
