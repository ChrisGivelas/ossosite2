import React from "react";
import SpinnerIcon from "../../assets/images/spinner.gif";

function Spinner() {
    return <img style={{width: 40}} src={SpinnerIcon} alt="spinner" />;
}

export default Spinner;
