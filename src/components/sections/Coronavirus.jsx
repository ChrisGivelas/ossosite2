import React from "react";

function Coronavirus() {
  return (
    <section id="coronavirus">
      <h1 className="section-title">Coronavirus Update</h1>
      <p className="section-description">
        Osso City Lighting will be officially reopening to our customers this
        coming Tuesday, June 9th. Regular store hours will be in effect. In
        order to keep everyone as safe as possible during these trying times, we
        ask that you please read and respect the following guidelines:
      </p>
      <hr />
      <p className="section-description">
        - If you are experiencing symptoms of illness, such as fever, cough, and
        heavy breathing, or are in self-isolation, we ask that you please
        refrain from visiting our store at this time.
      </p>
      <hr />
      <p className="section-description">
        - We will have reduced store capacity. We encourage only one (1) person
        per household, but will allow up to two (2) if necessary, for a maximum
        of no more than six (6) customers in the store at one time. No children,
        please.
      </p>
      <hr />
      <p className="section-description">
        - Wearing a mask is mandatory, for your safety and ours. Anyone not
        wearing a mask will not be permitted to enter the store. Hand sanitizer
        will be available at the entrance and throughout the showroom.
      </p>
      <hr />
      <p className="section-description">
        - Once inside, you will be accompanied at all times by one of our staff
        members.
      </p>
      <hr />
      <p className="section-description">
        - Respect social distancing; keep two meters away from others.
      </p>
      <hr />
      <p className="section-description">
        - Please refrain from touching anything on display while in the store.
      </p>
      <hr />
      <p className="section-description">
        - Restrooms are currently unavailable.
      </p>
      <hr />
      <p className="section-description">
        - Large house orders will be by appointment only.
      </p>
      <hr />
      <p className="section-description">
        - For method of payment, we encourage credit cards or debit cards, if
        possible.
      </p>
      <hr />
      <p className="section-description">
        - If ordering products via email or by phone, please direct emails to
        info@ossolighting.ca or call us at 905-404-6776. Once confirmed, we will
        ask you to call in with your preferred card information.
      </p>
      <hr />
      <p className="section-description">
        - All sales are final. Returns will not be accepted; exceptions will be
        made for defective items only.
      </p>
      <hr />
      <p className="section-description">
        - For order pickups, please proceed to our shipping doors located at the
        back of building and a staff member will be there to assist you. Please
        have your invoice present at this time. If your order was placed through
        email, your invoice will be attached to your order.
      </p>
      <hr />
      <p className="section-description">
        We kindly ask that you be considerate of your fellow customers and our
        staff members as we navigate through these uncertain times. We are all
        in this together. Stay safe and stay healthy. We look forward to seeing
        you soon! Thank you for supporting your local business!
      </p>
    </section>
  );
}

export default Coronavirus;
