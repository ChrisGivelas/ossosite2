import CHANDELIER from "./CHANDELIER.jpg";
import LINEAR from "./LINEAR.jpg";
import OUTDOOR from "./OUTDOOR.jpg";
import PENDANT from "./PENDANT.jpg";
import SCONCE from "./SCONCE.jpg";
import VANITY from "./VANITY.jpg";

export {CHANDELIER, LINEAR, OUTDOOR, PENDANT, SCONCE, VANITY};
