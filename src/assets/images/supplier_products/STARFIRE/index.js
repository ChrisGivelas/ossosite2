import CEILING from "./CEILING.jpg";
import CHANDELIER from "./CHANDELIER.jpg";
import PENDANT from "./PENDANT.jpg";
import SCONCE from "./SCONCE.jpg";

export {CEILING, CHANDELIER, PENDANT, SCONCE};
