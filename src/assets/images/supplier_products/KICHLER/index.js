import CEILING from "./CEILING.jpg";
import CHANDELIER from "./CHANDELIER.jpg";
import FAN from "./FAN.jpg";
import LINEAR from "./LINEAR.jpg";
import OUTDOOR from "./OUTDOOR.jpg";
import PENDANT from "./PENDANT.jpg";
import SCONCE from "./SCONCE.jpg";
import VANITY from "./VANITY.jpg";

export {CEILING, CHANDELIER, FAN, LINEAR, OUTDOOR, PENDANT, SCONCE, VANITY};
