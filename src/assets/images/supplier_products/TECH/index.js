import CEILING from "./CEILING.jpg";
import CHANDELIER from "./CHANDELIER.jpg";
import LAMP from "./LAMP.jpg";
import LINEAR from "./LINEAR.jpg";
import OUTDOOR from "./OUTDOOR.jpg";
import PENDANT from "./PENDANT.jpg";
import SCONCE from "./SCONCE.jpg";
import TRACK from "./TRACK.jpg";
import VANITY from "./VANITY.jpg";

export {CEILING, CHANDELIER, LAMP, LINEAR, OUTDOOR, PENDANT, SCONCE, TRACK, VANITY};
