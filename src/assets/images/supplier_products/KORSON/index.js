import ART from "./ART.jpg";
import CABINET from "./CABINET.jpg";
import COFFETABLE from "./COFFEE TABLE.jpg";
import CONSOLETABLE from "./CONSOLE TABLE.jpg";
import DRAWER from "./DRAWER.jpg";
import OTTOMAN from "./OTTOMAN.jpg";
import STOOL from "./STOOL.jpg";

export {ART, CABINET, COFFETABLE, CONSOLETABLE, DRAWER, OTTOMAN, STOOL};
