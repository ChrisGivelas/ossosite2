import COFFEETABLE from "./COFFEE TABLE.jpg";
import CONSOLETABLE from "./CONSOLE TABLE.jpg";
import OTTOMAN from "./OTTOMAN.jpg";
import STOOL from "./STOOL.jpg";

export {COFFEETABLE, CONSOLETABLE, OTTOMAN, STOOL};
