import CEILING from "./CEILING.jpg";
import CHANDELIER from "./CHANDELIER.jpg";
import OUTDOOR from "./OUTDOOR.jpg";
import PENDANT from "./PENDANT.jpg";
import SCONCE from "./SCONCE.jpg";
import VANITY from "./VANITY.jpg";

export {CEILING, CHANDELIER, OUTDOOR, PENDANT, SCONCE, VANITY};
