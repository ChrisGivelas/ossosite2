import CEILING from "./CEILING.jpg";
import CHANDELIER from "./CHANDELIER.jpg";
import LAMP from "./LAMP.jpg";
import OUTDOOR from "./OUTDOOR.jpg";
import PENDANT from "./PENDANT.jpg";
import RECESSED from "./RECESSED.jpg";
import SCONCE from "./SCONCE.jpg";
import TRACK from "./TRACK.jpg";
import VANITY from "./VANITY.jpg";

export {CEILING, CHANDELIER, LAMP, OUTDOOR, PENDANT, RECESSED, SCONCE, TRACK, VANITY};
