import FLOORLAMP from "./FLOOR LAMP.jpg";
import TABLELAMP from "./TABLE LAMP.jpg";
import TASKLAMP from "./TASK LAMP.jpg";

export {FLOORLAMP, TABLELAMP, TASKLAMP};
