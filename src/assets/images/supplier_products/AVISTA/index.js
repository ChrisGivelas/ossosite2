import CEILING from "./CEILING.jpg";
import CHANDELIER from "./CHANDELIER.jpg";
import PENDANT from "./PENDANT.jpg";
import VANITY from "./VANITY.jpg";

export {CEILING, CHANDELIER, PENDANT, VANITY};
