import CEILING from "./CEILING.jpg";
import CHANDELIER from "./CHANDELIER.jpg";
import LANDSCAPE from "./LANDSCAPE.jpg";
import LINEAR from "./LINEAR.jpg";
import MIRROR from "./MIRROR.jpg";
import PENDANT from "./PENDANT.jpg";
import RECESSED from "./RECESSED.jpg";
import TRACK from "./TRACK.jpg";
import VANITY from "./VANITY.jpg";

export {CEILING, CHANDELIER, LANDSCAPE, LINEAR, MIRROR, PENDANT, RECESSED, TRACK, VANITY};
