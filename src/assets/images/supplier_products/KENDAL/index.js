import LAMP from "./LAMP.jpg";
import PENDANT from "./PENDANT.jpg";
import TORCHIER from "./TORCHIER.jpg";

export {LAMP, PENDANT, TORCHIER};
